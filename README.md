---
author: Kiara Jouhanneau | @kajou.design | kajou.design@gmail.com | ko-fi.com/kajoudesign    
title: Cahors  
project: paged.js sprint
book format: letter
book orientation: portrait
version: v1.0

Inspired by "Debates contemporâneos em direito médico e da saúde" (see PDF file)
---
  
## → fonts/type designers :  
### Asul by [Mariela Monsalve](https://fonts.google.com/specimen/Asul)  
>*Asul can be described as a baroque humanist typface; it has semi-serifs and it is a type revival project developed for editorial use. It is based on a typeface found in some Argentinian books and magazines from the early 20th century.* 
### Noto (Sans and Serif) by [Google Fonts](https://fonts.google.com/noto/fonts)  
>*Noto is a global font collection for writing in all modern and ancient languages. Noto Sans is an unmodulated (“sans serif”) design for texts in the Latin, Cyrillic and Greek scripts, which is also suitable as the complementary choice for other script-specific Noto Sans fonts. It has italic styles, multiple weights and widths, and 3,741 glyphs.  
To contribute, see [(github.com/googlefonts/noto-source)]* 
### Poppins by [Jonny Pinhorn (Latin) and Ninad Kale (Devanagari)](https://fonts.google.com/specimen/Poppins?query=poppins)  
>*Geometric sans serif typefaces have been a popular design tool ever since these actors took to the world’s stage. Poppins is one of the new comers to this long tradition. With support for the Devanagari and Latin writing systems, it is an internationalist take on the genre. Each letterform is nearly monolinear, with optical corrections applied to stroke joints where necessary to maintain an even typographic color. The Devanagari base character height and the Latin ascender height are equal; Latin capital letters are shorter than the Devanagari characters, and the Latin x-height is set rather high.
The Devanagari is designed by Ninad Kale. The Latin is by Jonny Pinhorn. To contribute, see [(github.com/itfoundry/poppins)]*
### IA Writer Mono by [Information Architects Inc.](https://github.com/iaolo/iA-Fonts)  
>*iA Writer comes bundled with iA Writer for Mac and iOS.This is a modification of IBM's Plex font. The upstream project is [here](https://github.com/IBM/plex)*
    

## → the following classes :  
>> **Component Basis**  
component-body  
component-front  
component-title  
content-opener-image  
decoration  
paragraph  

>> **Component: Long**  
biography  
case-study  
feature   
long  

>> **Component: Shortboxes**  
note  
reminder  
short  
tip  
warning  

>> **Features: Closing**  
key-term  
key-terms  
references  
review-activity  
summary  

>> **Page: Chapter**  
chapter  
chapter-number  
focus-questions  
introduction  
learning-objectives  
outline  
outline-remake  

>> **Page: Part**  
part  
part-number  
outline  


>> **Page: TOC**  
ct  
toc   
toc-body  
toc-chapter  
toc-part  
name  

>> **Others**    
restart-numbering  
running-left  
running-right  
start-right  

